{
  description = "Test flake";

  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }: let
    ourHello = {
      lib,
      stdenv,
      fetchurl,
    }:
      stdenv.mkDerivation (finalAttrs: {
        pname = "hello";
        version = "2.12.1";
        src = fetchurl {
          url = "mirror://gnu/hello/hello-${finalAttrs.version}.tar.gz";
          sha256 = "sha256-jZkUKv2SV28wsM18tCqNxoCZmLxdYH2Idh9RLibH2yA=";
        };
        patches = [./hello.patch];
        meta = with lib; {
          description = "Hello package";
          license = licenses.gpl3Plus;
          platforms = platforms.all;
        };
      });
  in
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      packages = {
        default = pkgs.callPackage ourHello {};
      };

      apps.default = {
        type = "app";
        program = "${self.packages.${system}.default}/bin/hello";
      };

      checks.default = self.packages.${system}.default;

      formatter = pkgs.alejandra;
    });
}
