# Nix Course

This is Nix course for Elektroline.

## Running Jupyuter

The course was supported with Jupyter notebooks and you can run them and see
Nix in action that way with:

```
nix run
```
