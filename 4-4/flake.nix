{
  description = "Test flake";

  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      packages = {
        default = pkgs.stdenv.mkDerivation rec {
          pname = "hello";
          version = "2.12.1";
          src = pkgs.fetchurl {
            url = "mirror://gnu/hello/hello-${version}.tar.gz";
            sha256 = "sha256-jZkUKv2SV28wsM18tCqNxoCZmLxdYH2Idh9RLibH2yA=";
          };
          patches = [./hello.patch];
          meta = with pkgs.lib; {
            description = "Hello package";
            license = licenses.gpl3Plus;
            platforms = platforms.all;
          };
        };
      };

      apps.default = {
        type = "app";
        program = "${self.packages.${system}.default}/bin/hello";
      };

      checks.default = self.packages.${system}.default;

      formatter = pkgs.alejandra;
    });
}
