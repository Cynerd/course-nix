{
  description = "Test flake";

  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }: let
    ourHello = {
      lib,
      stdenv,
      fetchurl,
    }:
      stdenv.mkDerivation (finalAttrs: {
        pname = "hello";
        version = "2.12.1";
        src = fetchurl {
          url = "mirror://gnu/hello/hello-${finalAttrs.version}.tar.gz";
          sha256 = "sha256-jZkUKv2SV28wsM18tCqNxoCZmLxdYH2Idh9RLibH2yA=";
        };
        patches = [./hello.patch];
        meta = with lib; {
          description = "Hello package";
          license = licenses.gpl3Plus;
          platforms = platforms.all;
        };
      });
  in
    {
      overlays.default = final: prev: {
        ourHello = final.callPackage ourHello {};
      };
    }
    // flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system}.extend self.overlays.default;
    in {
      packages = {
        inherit (pkgs) ourHello;
        default = self.packages.${system}.ourHello;
      };

      devShells.default = pkgs.mkShell {
        packages = [pkgs.ourHello];
        shellHook = "echo Welcome to development environment.";
      };

      apps.default = {
        type = "app";
        program = "${pkgs.ourHello}/bin/hello";
      };

      checks.default = pkgs.ourHello;

      formatter = pkgs.alejandra;
    });
}
