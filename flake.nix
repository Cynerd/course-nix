{
  description = "Nix course in jupyenv";

  outputs = {
    self,
    flake-utils,
    nixpkgs,
  }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        pkgs = nixpkgs.legacyPackages.${system};
        spkgs = self.packages.${system};

        pyenv = pkgs.python3.buildEnv.override {
          extraLibs = [
            ((pkgs.python3Packages.nix-kernel.overrideAttrs (_: {
                patches = [./.hacks/nix-kernel-Drop-continuation-prompt.patch];
              }))
              .override (_: {
                nix = pkgs.nix.overrideAttrs (oldAttrs: {
                  patches = oldAttrs.patches ++ [./.hacks/nix-Prompt.patch];
                });
              }))
          ];
        };
      in rec {
        packages.default = pkgs.jupyter.override (_: {
          definitions = {
            Nix = {
              displayName = "Nix";
              argv = [pyenv.interpreter "-m" "nix-kernel" "-f" "{connection_file}"];
              language = "nix";
              logo32 = null;
              logo64 = null;
            };
          };
        });
        apps.default = {
          type = "app";
          program = "${spkgs.default}/bin/jupyter-lab";
          #program = pyenv.interpreter;
        };
        formatter = pkgs.alejandra;
      }
    );
}
