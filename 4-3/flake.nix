{
  description = "Test flake";

  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      packages = {
        default = pkgs.hello;
      };

      apps.default = {
        type = "app";
        program = "${self.packages.${system}.default}/bin/hello";
      };

      formatter = pkgs.alejandra;
    });
}
