{
  description = "Test flake";

  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      packages = {
        default = pkgs.hello.override (oldAttrs: {
          inherit (pkgs.pkgsi686Linux) stdenv;
        });
      };

      apps.default = {
        type = "app";
        program = "${self.packages.${system}.default}/bin/hello";
      };

      checks.default = self.packages.${system}.default;

      formatter = pkgs.alejandra;
    });
}
